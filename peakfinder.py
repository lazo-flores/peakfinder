""" PyROOT TSpectrum example for Vera.
    """
from ROOT import gROOT, TCanvas, TH1F, TH1, TSpectrum, TFile
import numpy as np

def get_peaks(histo,sigma=5,opt="",threshold=0.0002,niter=20):
    """ get_peaks(). Takes in a histogram, the peak width, the treshold
        and the number of iterations. 
        Updates the histogram with the position of the peaks and the background.
        Returns a list of the x and y coordinates of each found peak.
        """
    """ To be used to hold the background subtracted data (the signal)"""
    histo_signal = histo.Clone('signal')
    spectrum = TSpectrum(niter)
    """ Find the background. b_niter is the parameter that tells the backgournd 
        finder method of TSpectrum how smooth the background should be. """
    b_niter = 60
    histo_bgnd = spectrum.Background(histo,b_niter)
    save_to_rootFile('test1',histo_bgnd)
    
    """ Now lets subtract the baseline """
    histo_signal.Add(histo_bgnd,-1)
    save_to_rootFile('test1',histo_signal)
    """ To be used to mark the peak candidates."""
    histo_signal_peaks = histo_signal.Clone('signal_w_peaks')
    """ Find the peaks. """
    nfound = spectrum.Search(histo_signal_peaks,sigma,opt,threshold)
    print 'Found', nfound, 'candidate peaks'
    save_to_rootFile('test1',histo_signal_peaks)
    """ Get the x and y positions of the the candidate peaks. """
    bufX, bufY = spectrum.GetPositionX(), spectrum.GetPositionY()
    """ Arrange the positions in a list to be returned. """
    pos = []
    for i in range(spectrum.GetNPeaks()):
        pos.append([bufX[i], bufY[i]])
    pos.sort()

    return pos, histo_signal

def get_data(fname):
    """ get_data('file_name'). Takes in the name of the data file (currently
        without the file extention). It returns a numpy array contatining the
        data and a list with the metadata (X-title, Y-title, X-multiplier, and 
        Y-multiplier).
        """
    file_name = '{!s}.asc'.format(fname)
    infile = open( file_name )
    count = 0
    data_ar = np.array([])
    for line in infile:
        count += 1
        if line.startswith('Total Data Points'):
            chan_len = np.array( [ int(i) for i in line.split('\t')[1:-1] ] )
        if line.startswith('X Axis Title'):
            x_titles = [ i.strip() for i in line.split('\t')[1:] ]
        if line.startswith('Y Axis Title'):
            y_titles = [ i.strip() for i in line.split('\t')[1:] ]
        if line.startswith('X Axis Multiplier'):
            x_mult = np.array( [ float(i) for i in line.split('\t')[1:] ] )
        if line.startswith('Y Axis Multiplier'):
            y_mult = np.array( [ float(i) for i in line.split('\t')[1:] ] )
        if count > 16:
            data_ar = np.append(data_ar, [float(line)])

    met_dat = [[x_titles[0], y_titles[0], x_mult[0], y_mult[0]],
               [x_titles[1], y_titles[1], x_mult[1], y_mult[1]],
               [x_titles[2], y_titles[2], x_mult[2], y_mult[2]],
               [x_titles[3], y_titles[3], x_mult[3], y_mult[3]]]
            
    ind1, ind2 = chan_len[0], sum(chan_len[0:2])
    ind3, ind4 = sum(chan_len[0:3]), sum(chan_len[0:4])
    ch0_dat = data_ar[0:ind1]
    ch1_dat = data_ar[ind1:ind2]
    ch2_dat = data_ar[ind2:ind3]
    ch3_dat = data_ar[ind3:ind4]
    data = np.array([ch0_dat, ch1_dat, ch2_dat, ch3_dat])
    
    return data, met_dat

def make_histo(data,metdat,hist_name):
    """ make_histos(data, metdat, histogram). Takes in the data, metadata
        and canvas. Using canvas for testing purposes.
        Returns the histogram pointer.
        """
    nlines = len(data)
    title = '{!s} v {!s}'.format(metdat[0],metdat[1])
    histo = TH1F( hist_name, title, nlines, 0.0, float(nlines) )
    for i in range(nlines):
        histo.SetBinContent(i,data[i]*metdat[3])
    histo.GetXaxis().SetTitle(metdat[0])
    histo.GetYaxis().SetTitle(metdat[1])
    """The next few lines are used to take a quick look at the histogra and 
        it shows how to to make a pdf out of a histogram."""
#    canvas = prep_canvas()
#    histo.Draw()
#    canvas.SaveAs('newplots/{!s}.pdf'.format(hist_name))
#    canvas.Clear()
    return histo

def prep_canvas():
    """prep_canvas(). Takes no arguments and it returs the canvas pointer.
        """
    gROOT.Reset()
    c0 = TCanvas( 'c0', 'Canvas', 2303, 0, 656, 700 )
    c0.SetRightMargin(0.15)
    return c0

def save_to_rootFile(fname, item):
    """ save_to_rootFile(item). Takes in root file name (without extention) and 
        pointer of item to be saved in root file. If the target file does not
        exist, it creates it. If the file exists, it adds the new item to it.
        Returns nothing.
        """
    rootfile = '{!s}.root'.format(fname)
    out_file = TFile(rootfile, 'UPDATE')
    item.Write()
    out_file.Close()

def calc_peaks_area(histo,ch1_len):
    """ Takes in the histogram containing the background subtracted signal and 
        it calculates the area.
        Returns areas."""
    sig_dat = np.array([])
    for i in range(0,ch1_len):
        sig_dat = np.append(sig_dat, [histo.GetBinContent(i)])
    print sig_dat, len(sig_dat)
    sig_grad = np.gradient(sig_dat)
    print sig_grad[960:970]

if __name__=="__main__":
    dat, metdata = get_data('0313-221-Rep24')
    print dat[0]
    print metdata[0]
    ch1_len = len(dat[0])
    print ch1_len
    h_channel1 = make_histo(dat[0],metdata[0],'channel_1')
    save_to_rootFile('test1',h_channel1)
    positions, sig_hist = get_peaks(h_channel1)
    print positions
    calc_peaks_area(sig_hist,ch1_len)
